syntax = "proto3";

package olimpapi;

import "google/protobuf/wrappers.proto";


// Статусы матча
enum EventState {
  NULL = 0;
  ACTIVE = 1;
  PAUSE = 2;
  STOPPED = 3;
  FINISHED = 4;
  UNKNOWN = 5;
}

// id и версия объекта для запроса со стороны клиента
message VersionedIdClient {
  int64 id = 1;
  google.protobuf.Int64Value version = 2; // Запрос версии данных
}

// id чемпионата и версия объекта для ответа со стороны сервера
message VersionedIdServer {
  int64 id = 1; // id объекта
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
}

// События для заданного чемпионата
message ChampEvents {
  int64 champId = 1; // Идентификатор чемпионата
  Champ champ = 2; // Название чемпионата
  repeated Event events = 3; // Список событий чемпионата
  google.protobuf.BoolValue deleted = 4; // Флаг "удален"
}

// данные по чемпионату
message Champ {
  int64 id = 1; // id чемпионата
  google.protobuf.StringValue name = 2; // название чемпионата
  google.protobuf.Int64Value sportId = 3; // Идентификатор вида спорта
  google.protobuf.StringValue sportName = 4; // Название спорта
  google.protobuf.StringValue comment = 5; // Комментарий
  google.protobuf.BoolValue stat = 6; // Флаг того что у чемпионата тип "Статистика"
  google.protobuf.BoolValue total = 7; // Флаг того что у чемпионата тип "Итоги" (**уточнить**)
  google.protobuf.Int64Value maxBet = 8; // Максимальная ставка
  google.protobuf.Int32Value order = 9; // Порядок
  google.protobuf.Int64Value startDate = 10; // дата начала
  google.protobuf.Int32Value eventsCount = 11; // дата окончания
  google.protobuf.BoolValue deleted = 12; // Флаг того что удален
}


message ChampEventsVersionedResponse {
  ChampEvents data = 1;  // События для заданного чемпионата
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
}

message ChampsEventsVersionedResponse {
  repeated ChampEvents data = 1; // События для заданного чемпионата
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
  repeated VersionedIdServer ids = 4; // id чемпионата и версия объекта для ответа со стороны сервера
}


// Информация о событии (матче)
message Event {
  int64 id = 1; // Идентификатор события
  google.protobuf.Int64Value parentId = 2; // Идентификатор родителя (возможно для событий типа "статистика" или "итоги")
  google.protobuf.StringValue name = 3; // Название события
  google.protobuf.Int64Value champId = 4; // Идентификатор чемпионата
  google.protobuf.StringValue champName = 5; // Название чемпионата
  google.protobuf.Int64Value sportId = 6; // Идентификатор вида спорта. Посмотреть вид спорта можно с помощью метода КЗ API [Sportslistal](https://xwiki.olimp.dev/bin/view/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D1%8B/KZ%20API/SPORT%20%28%D1%81%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D1%8B%20%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5%20%D1%8D%D0%BD%D0%B4%D0%BF%D0%BE%D0%B8%D0%BD%D1%82%D1%8B%20%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D1%85%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D1%81%D0%BE%20sport*%29/Sportslistal/)
  google.protobuf.StringValue sportName = 7; // Название вида спорта
  EventState state = 8; // Статус матча
  google.protobuf.Int64Value start = 9; // Дата и время начала матча в формате UNIX timestamp
  google.protobuf.Int32Value order = 10; // Сортировка
  google.protobuf.Int64Value brId = 11; // Идентификатор матча у betRadar
  google.protobuf.Int64Value brTeam1Id = 12; // Идентификатор домашней (первой) команды (участника) а у betRadar
  google.protobuf.Int64Value brTeam2Id = 13; // Идентификатор гостевой (второй) команды (участника) а у betRadar
  google.protobuf.StringValue comment = 14; // Комментарий
  google.protobuf.Int64Value maxBet = 15; // Ограничение максимальной ставки
  google.protobuf.Int32Value specVal = 16; // Значение, в котором кодируются некоторые значения для матча. К примеру в матче можно принимать только ординары
  google.protobuf.StringValue score = 17; // Счет в матче
  google.protobuf.Int32Value outcomeCount = 18; // Количество исходов для матча
  repeated Market markets = 19;  // Список рынков (маркетов) ставок.
  google.protobuf.BoolValue deleted = 20; // Флаг того что "Удалено"
}


message EventVersionedResponse {
  Event data = 1; //
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
}

// Данные о событиях в чемпионате за сутки
message Events24Champ {
  int64 champId = 1; // Идентификатор чемпионата
  google.protobuf.StringValue champName = 2; // Название чемпионата
  repeated Event events = 3; // Список событий чемпионата
}

message Events24PageResponse {
  int32 page = 1; // Страница в пагинации (должно быть меньше size)
  int32 size = 2; // Количество страниц
  int32 total = 3; // Всего записей
  repeated Events24Sport data = 4; // Вывод данных
}

// Выводит события по виду спорта за 24 часа
message Events24Sport {
  int64 sportId = 1; // Идентификатор вида спорта. Посмотреть вид спорта можно с помощью метода КЗ API [Sportslistal](https://xwiki.olimp.dev/bin/view/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D1%8B/KZ%20API/SPORT%20%28%D1%81%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D1%8B%20%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5%20%D1%8D%D0%BD%D0%B4%D0%BF%D0%BE%D0%B8%D0%BD%D1%82%D1%8B%20%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D1%85%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D1%81%D0%BE%20sport*%29/Sportslistal/)
  google.protobuf.StringValue sportName = 2; // Название спорта
  repeated Events24Champ champs = 3; // Данные о событиях в чемпионате за сутки (список)
}

message EventsVersionedResponse {
  repeated Event data = 1;
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
  repeated VersionedIdServer ids = 4; // id события и версия объекта для ответа со стороны сервера
}

// Рынок ставок
message Market {
  int32 category = 1; // категория ставок
  google.protobuf.StringValue groupName = 2; // название группы исходов
  google.protobuf.Int32Value groupPos = 3; // позиция группы исходов
  repeated Outcome outcomes = 4; // исходы
  google.protobuf.BoolValue deleted = 5; // Флаг "удален"
}

// данные об исходы
message Outcome {
  int64 id = 1; // Идентификатор исхода
  google.protobuf.StringValue name = 2; // Название исхода
  google.protobuf.StringValue shortName = 3;  // Название вида спорта
  google.protobuf.Int32Value outcomeType = 4; // Тип исхода
  google.protobuf.Int32Value pos = 5; // Позиция исхода
  google.protobuf.DoubleValue value = 6; // Коэффициент
  google.protobuf.Int64Value maxbet = 7;  // Максимальная ставка на события матча в тенге
  google.protobuf.Int64Value maxmisc = 8; // Максимальная выплата. Обозначает сколько игрок может выиграть на этом событии
  google.protobuf.Int32Value usemaxpay = 9; // Флаг использования ограничение на максимальный платеж (видимо)
  google.protobuf.Int64Value maxpayment = 10; // Максимальный платеж
  google.protobuf.StringValue basketId = 11;  // Идентификатор для корзины
  google.protobuf.BoolValue deleted = 12;  // Флаг "удален"
}

// запрос одной из страниц пагинации в выдаче Events24
message Events24PageRequest {
  int32 lang = 1; // Идентификатор языка
  google.protobuf.Int64Value sportId = 2; // Идентификатор вида спорта. Посмотреть вид спорта можно с помощью метода КЗ API [Sportslistal](https://xwiki.olimp.dev/bin/view/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D1%8B/KZ%20API/SPORT%20%28%D1%81%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D1%8B%20%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5%20%D1%8D%D0%BD%D0%B4%D0%BF%D0%BE%D0%B8%D0%BD%D1%82%D1%8B%20%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D1%85%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D1%81%D0%BE%20sport*%29/Sportslistal/)
  google.protobuf.Int32Value page = 3; // Страница пагинации
  google.protobuf.Int32Value size = 4; // Количество страниц в пагинации
}

// Выдача данных по чемпионатам для данного вида спорта
message SportChamps {
  int64 sportId = 1; // Идентификатор вида спорта. Посмотреть вид спорта можно с помощью метода КЗ API [Sportslistal](https://xwiki.olimp.dev/bin/view/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D1%8B/KZ%20API/SPORT%20%28%D1%81%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D1%8B%20%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5%20%D1%8D%D0%BD%D0%B4%D0%BF%D0%BE%D0%B8%D0%BD%D1%82%D1%8B%20%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D1%85%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D1%81%D0%BE%20sport*%29/Sportslistal/) 
  Sport sport = 2; // Название вида спорта
  repeated Champ champs = 3; // Список данных о чемпионатах
  google.protobuf.BoolValue deleted = 4; // Флаг "удален"
}

message SportsChampsVersionedResponse {
  repeated SportChamps data = 1; // Список чемпионатов по видам спорта
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
}

message SportChampsVersionedResponse {
  SportChamps data = 1; // Список чемпионатов по виду спорта
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3;  // Текущая версия данных на сервере
}

// События для вида спорта
message SportEvents {
  int64 sportId = 1; // Идентификатор вида спорта. Посмотреть вид спорта можно с помощью метода КЗ API [Sportslistal](https://xwiki.olimp.dev/bin/view/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D1%8B/KZ%20API/SPORT%20%28%D1%81%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D1%8B%20%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5%20%D1%8D%D0%BD%D0%B4%D0%BF%D0%BE%D0%B8%D0%BD%D1%82%D1%8B%20%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D1%85%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D1%81%D0%BE%20sport*%29/Sportslistal/) 
  Sport sport = 2;  // Название спорта.
  repeated Event events = 3; // Список событий
  google.protobuf.BoolValue deleted = 4; // Флаг "удален"
}

// События для вида спорта версионированные
message SportEventsVersionedResponse {
  SportEvents data = 1; // События для вида спорта
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
}

//  Данные о виде спорта
message Sport {
  int64 id = 1; //  Идентификатор вида спорта. Посмотреть вид спорта можно с помощью метода КЗ API [Sportslistal](https://xwiki.olimp.dev/bin/view/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D1%8B/KZ%20API/SPORT%20%28%D1%81%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D1%8B%20%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5%20%D1%8D%D0%BD%D0%B4%D0%BF%D0%BE%D0%B8%D0%BD%D1%82%D1%8B%20%D0%BD%D0%B0%D0%B7%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D1%85%20%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D1%81%D0%BE%20sport*%29/Sportslistal/) 
  google.protobuf.StringValue name = 2; // Название вида спорта
  google.protobuf.Int32Value pos = 3; // Позиция в сортировке
  google.protobuf.Int32Value count = 4;  // Количество видов спорта
  google.protobuf.BoolValue deleted = 5;  // Флаг "удален".
}

 //  Данные о виде спорта с версионированием
message SportVersionedResponse {
  Sport data = 1; //  Данные о виде спорта
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
}

 //  Данные о видах спорта с версионированием
message SportsVersionedResponse {
  repeated Sport data = 1;  //  Данные о виде спорта (список)
  google.protobuf.Int64Value versionFrom = 2; // Версия от которой передаются различия в данных (версия отданная клиентом)
  google.protobuf.Int64Value version = 3; // Текущая версия данных на сервере
}

// Список чемпионатов по видам спорта
message SportChampsList {
  repeated SportChamps items = 1; // Список чемпионатов по виу спорта
}

// Список событий для вида чемпионата
message ChampEventsList {
  repeated ChampEvents items = 1;
}

// Список событий 
message EventList {
  repeated Event items = 1; // список событий
}

// Список видов спорта
message SportList {
  repeated Sport items = 1; // Список видов спорта
}
